const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const http = require('http');
const socketIo = require('socket.io');

const app = express();
const PORT = 3001;

app.use(cors());

mongoose.connect(
  'mongodb+srv://pujaribalaji:balaji@cluster0.j7f9qun.mongodb.net/gulak',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log('Connected to MongoDB');
});

const urlSchema = new mongoose.Schema({
  url: {
    type: String,
    unique: true,
    required: true,
  },
});

const UrlModel = mongoose.model('Url', urlSchema);

app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('Welcome to Gulak App');
});

app.post('/redeem', async (req, res) => {
  const {url} = req.body;

  try {
    const existingUrl = await UrlModel.findOne({url});
    if (existingUrl) {
      return res.status(400).json({error: 'URL already exists'});
    }

    // Additional validation for Instagram URLs if needed
    if (!isValidInstagramUrl(url)) {
      return res.status(400).json({error: `Invalid Instagram URL: ${url}`});
    }

    const newUrl = new UrlModel({url});
    await newUrl.save();

    // Notify connected clients about the new data (if using Socket.io)
    io.emit('newData', newUrl);

    return res.json({success: true, message: 'Request sent successfully'});
  } catch (error) {
    console.error(error);
    return res.status(500).json({error: 'Internal server error'});
  }
});

// Function to check if a URL is a valid Instagram profile URL
function isValidInstagramUrl(url) {
  const instagramUrlPattern =
    /^(https?:\/\/)?(www\.)?instagram\.com\/[a-zA-Z0-9_.]+\/?$/;
  return instagramUrlPattern.test(url);
}

const server = http.createServer(app);
const io = socketIo(server);

// Add this block to listen for socket connections
io.on('connection', socket => {
  console.log('Client connected');

  socket.on('disconnect', () => {
    console.log('Client disconnected');
  });
});

app.get('/urls', async (req, res) => {
  try {
    const urls = await UrlModel.find();
    res.json(urls);
  } catch (error) {
    res.status(500).json({error: 'Internal Server Error'});
  }
});

server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

// Rest of your server code...

import React, {useRef, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Button,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import {useNavigation} from '@react-navigation/native';
import LottieView from 'lottie-react-native';
import {ScrollView} from 'react-native-gesture-handler';
const {height, width} = Dimensions.get('window');

const Landing = () => {
  const navigation = useNavigation();
  const lottieRef = useRef(null);

  useEffect(() => {
    // Start playing the Lottie animation when the component mounts
    if (lottieRef.current) {
      lottieRef.current.play();
    }
  }, []);

  const handlePlayNowPress = () => {
    // Navigate to the 'Game' screen
    navigation.navigate('Game');
  };
  const handlePlayPress = () => {
    navigation.navigate('Quotes');
  };
  return (
    <>
      <LinearGradient
        style={styles.radialGradient}
        colors={['#FD5', '#FD5', '#FF543E', '#DD4282', '#C837AB']}
        start={{x: -0.0814, y: 1.1461}}
        end={{x: 0.7, y: 0.8127}}>
        <ScrollView>
          <View style={{flex: 1}}>
            <Text
              style={{
                fontSize: 30,
                color: 'white',
                textAlign: 'center',
                top: 20,
                fontFamily: 'Poppins-Regular',
              }}>
              Gulak
            </Text>
            <Image
              source={require('../assets/ad.png')}
              style={{justifyContent: 'center', alignSelf: 'center', top: 40}}
            />

            <View
              style={{
                paddingTop: 5,
                paddingBottom: 5,
                backgroundColor: 'white',
                top: 70,
              }}>
              <View style={{display: 'flex', flexDirection: 'row', gap: 10}}>
                <LottieView
                  ref={lottieRef}
                  source={require('../Animation - 1704179350964.json')} // Replace with your Lottie animation file
                  autoPlay={true}
                  loop={true} // Set loop to true for continuous playback
                  style={{width: wp(35), height: wp(35), alignSelf: 'center'}} // Set dimensions as per your requirement
                />
                <View style={{gap: 10}}>
                  <Text
                    style={{
                      color: '#F04C5B',
                      fontSize: hp(2.4),
                      textAlign: 'center',
                      fontFamily: 'Poppins-Regular',
                    }}>
                    Get {'\n'}
                    <Text
                      style={{
                        color: '#F04C5B',
                        fontSize: hp(3.5),
                        fontFamily: 'Poppins-BlackItalic',
                      }}>
                      Followers
                    </Text>
                    {'\n'} & {'\n'}{' '}
                    <Text
                      style={{
                        color: '#F04C5B',
                        fontSize: hp(3.5),
                        fontFamily: 'Poppins-BlackItalic',
                      }}>
                      Likes
                    </Text>
                  </Text>
                  <TouchableOpacity onPress={handlePlayNowPress}>
                    <View
                      style={{
                        width: wp(45),
                        height: hp(5),
                        borderRadius: 10,
                        backgroundColor: '#F04C5B',
                        justifyContent: 'center',
                      }}>
                      <Text
                        style={{
                          textAlign: 'center',
                          fontSize: hp(2.5),
                          color: 'white',
                          fontWeight: 'bold',
                        }}>
                        Play Now!
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={{gap: 5}}>
                  <Image
                    source={require('../assets/insta.png')}
                    style={{width: 56, height: 50}}
                  />
                  <Image
                    source={require('../assets/follower.png')}
                    style={{
                      transform: [{rotate: '20deg'}],
                      marginLeft: -10,
                      width: 40,
                      height: 40,
                      marginTop: 10,
                    }}
                  />
                  <Image
                    source={require('../assets/like.png')}
                    style={{
                      transform: [{rotate: '-20deg'}],
                      marginLeft: 10,
                      marginTop: 10,
                      width: 40,
                      height: 40,
                    }}
                  />
                </View>
              </View>
            </View>

            <View style={{gap: 20, marginBottom: 100}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignSelf: 'center',
                  gap: 50,
                  top: 100,
                }}>
                <TouchableOpacity onPress={handlePlayPress}>
                  <View
                    style={{
                      padding: 22,
                      backgroundColor: 'white',
                      borderRadius: 10,
                    }}>
                    <Image
                      source={require('../assets/hindi.png')}
                      style={{
                        width: wp(16),
                        height: hp(9),
                        alignSelf: 'center',
                      }}
                    />
                    <Text
                      style={{
                        fontSize: hp(2),

                        color: 'rgba(121, 72, 202, 1)',
                        fontFamily: 'Poppins-Bold',
                      }}>
                      Hindi Quotes
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={handlePlayPress}>
                  <View
                    style={{
                      padding: 22,
                      paddingLeft: 5,
                      paddingRight: 5,
                      backgroundColor: 'white',
                      borderRadius: 10,
                    }}>
                    <Image
                      source={require('../assets/motivation.png')}
                      style={{
                        width: wp(16),
                        height: hp(9),
                        alignSelf: 'center',
                      }}
                    />
                    <Text
                      style={{
                        fontSize: hp(2),
                        fontFamily: 'Poppins-Bold',
                        color: 'rgba(121, 72, 202, 1)',
                      }}>
                      Motivation Quotes
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignSelf: 'center',
                  gap: 50,
                  top: 100,
                }}>
                <TouchableOpacity onPress={handlePlayPress}>
                  <View
                    style={{
                      padding: 22,
                      paddingRight: 25,
                      paddingLeft: 25,
                      backgroundColor: 'white',
                      borderRadius: 10,
                    }}>
                    <Image
                      source={require('../assets/gita.png')}
                      style={{
                        width: wp(16),
                        height: hp(9),
                        alignSelf: 'center',
                      }}
                    />
                    <Text
                      style={{
                        fontSize: hp(2),
                        fontFamily: 'Poppins-Bold',
                        color: 'rgba(121, 72, 202, 1)',
                      }}>
                      Gita Quotes
                    </Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={handlePlayPress}>
                  <View
                    style={{
                      padding: 22,
                      paddingLeft: 10,
                      paddingRight: 17,
                      backgroundColor: 'white',
                      borderRadius: 10,
                    }}>
                    <Image
                      source={require('../assets/success.png')}
                      style={{
                        width: wp(16),
                        height: hp(9),
                        alignSelf: 'center',
                      }}
                    />
                    <Text
                      style={{
                        fontSize: hp(2),
                        fontFamily: 'Poppins-Bold',
                        color: 'rgba(121, 72, 202, 1)',
                      }}>
                      Success Quotes
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            <Image
              source={require('../assets/ad.png')}
              style={{
                justifyContent: 'center',
                alignSelf: 'center',
                marginTop: 40,
                marginBottom: 30,
              }}
            />
          </View>
        </ScrollView>
      </LinearGradient>
    </>
  );
};

export default Landing;

const styles = StyleSheet.create({
  radialGradient: {
    flex: 1,
  },
});

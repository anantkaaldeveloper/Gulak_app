import {StyleSheet, Text, View, Image} from 'react-native';
import React from 'react';
import {ScrollView} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Quotes = () => {
  return (
    <LinearGradient
      style={styles.radialGradient}
      colors={['#FD5', '#FD5', '#FF543E', '#DD4282', '#C837AB']}
      start={{x: -0.0814, y: 1.1461}}
      end={{x: 0.7, y: 0.8127}}>
      <ScrollView>
        <View style={{flex: 1}}>
          <Text
            style={{
              fontSize: 30,
              color: 'white',
              textAlign: 'center',
              top: 20,
              fontFamily: 'Poppins-Regular',
            }}>
            Gulak
          </Text>
          <Image
            source={require('../assets/ad.png')}
            style={{justifyContent: 'center', alignSelf: 'center', top: 40}}
          />
          <View style={{gap: 30, top: 70}}>
            <View
              style={{
                width: wp(80),
                height: hp(15),
                backgroundColor: 'white',
                justifyContent: 'center',
                alignSelf: 'center',
                borderRadius: 10,
              }}></View>
            <View
              style={{
                width: wp(80),
                height: hp(15),
                backgroundColor: 'white',
                justifyContent: 'center',
                alignSelf: 'center',
                borderRadius: 10,
              }}></View>
          </View>
          <Image
            source={require('../assets/ad.png')}
            style={{justifyContent: 'center', alignSelf: 'center', top: 100}}
          />
          <View style={{gap: 30, top: 120, marginBottom: 120}}>
            <View
              style={{
                width: wp(80),
                height: hp(15),
                backgroundColor: 'white',
                justifyContent: 'center',
                alignSelf: 'center',
                borderRadius: 10,
              }}></View>
            <View
              style={{
                width: wp(80),
                height: hp(15),
                backgroundColor: 'white',
                justifyContent: 'center',
                alignSelf: 'center',
                borderRadius: 10,
              }}></View>
          </View>
          <Image
            source={require('../assets/ad.png')}
            style={{
              justifyContent: 'center',
              alignSelf: 'center',
              top: 20,
              marginBottom: 40,
            }}
          />
        </View>
      </ScrollView>
    </LinearGradient>
  );
};

export default Quotes;

const styles = StyleSheet.create({
  radialGradient: {
    flex: 1,
  },
});
